package example

class Note {

  String name

  static constraints = {
  }

  String toString(){
    name
  }

}
