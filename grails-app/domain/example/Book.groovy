package example

class Book {

    String name

    static constraints = {
    }

    String toString(){
      name
    }
}
