package example

import grails.events.annotation.Subscriber
import grails.gorm.transactions.Transactional

@Transactional
class NoteService {

    @Subscriber('myevent')
    Note respondToEvent(Book book) {
      println "Responding to $book"

      String bookName = book.name
      new Note(name: "handling book $bookName").save()
    }
}
