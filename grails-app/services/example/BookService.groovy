package example

import grails.events.annotation.Publisher
import grails.gorm.transactions.Transactional

@Transactional
class BookService {

    @Publisher('myevent')
    Book doSomething() {
        new Book(name: 'test').save()
    }
}
