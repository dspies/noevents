package example

import grails.testing.mixin.integration.Integration
import grails.transaction.*
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Specification

@Integration
@Rollback
class BookServiceIntegrationSpec extends Specification {

    @Autowired
    BookService bookService

    def setup() {
    }

    def cleanup() {
    }

    void "test when book is created a note is created"() {
        when: "book is created"
        Book book = bookService.doSomething()

        then:
        book.name == 'test'

        and: 'note exists'
        Note.count() == 1
    }
}
